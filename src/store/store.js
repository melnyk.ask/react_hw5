import { configureStore } from "@reduxjs/toolkit";
import homeSlice from "../features/home/homeSlice";
import modalSlice from "../features/modal/modalSlice";
import cartSlice from "../features/cart/cartSlice";

export const store = configureStore({
    reducer: {
        home: homeSlice,
        modal: modalSlice,
        cart: cartSlice,
    },
});