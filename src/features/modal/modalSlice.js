import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    visible: false,
    shadow: false
};

export const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        setVisibleOn: (state) => {
            state.visible = true;
            state.shadow = true;
        },
        setVisibleOff: (state) => {
            state.visible = false;
            state.shadow = false;
        },
    }
});

export const {setVisibleOn, setVisibleOff} = modalSlice.actions;
export default modalSlice.reducer;