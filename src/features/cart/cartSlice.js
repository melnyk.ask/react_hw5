import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    numberInCart: 0,
    cart: [],
}

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        add: (state, action) => {
            //payload - это ID товара, добавляемого в корзину
            state.cart.push(action.payload);
            state.numberInCart = state.numberInCart + 1;
            localStorage.setItem("cart_toolkit", JSON.stringify(state.cart));
            localStorage.setItem("all_cart_toolkit", state.numberInCart);
        },
        remove: (state, action) => {
            let arr = state.cart;
            console.log("action.payload", action.payload);
            // получаем индекс в массиве товара по ID
            console.log(arr);

            let index = arr.indexOf(action.payload);
            console.log(index);

            arr.splice(index, 1);
            state.cart = arr;

            state.numberInCart = state.numberInCart - 1;
            localStorage.setItem("cart_toolkit", JSON.stringify(state.cart));
            localStorage.setItem("all_cart_toolkit", state.numberInCart);
        },
        sync: (state) => {
            if (localStorage.getItem("all_cart_toolkit")) { 
                state.numberInCart = +localStorage.getItem("all_cart_toolkit");
            }
            if (localStorage.getItem("cart_toolkit")) { 
                state.cart = JSON.parse(localStorage.getItem("cart_toolkit"));
            }
        },
        checkout: (state) => {
            state.cart = [];
            state.numberInCart = 0;
            if (localStorage.getItem("all_cart_toolkit")) { 
                // state.numberInCart = 0;
                localStorage.setItem("all_cart_toolkit", state.numberInCart);
            }
            if (localStorage.getItem("cart_toolkit")) { 
                localStorage.setItem("cart_toolkit", JSON.stringify(state.cart));
                // state.cart = [];
            }
        },
    }
});

export const { add, remove, sync, checkout } = cartSlice.actions;
export default cartSlice.reducer;