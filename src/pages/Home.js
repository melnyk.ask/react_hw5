import Products from "../components/Products.js";
// import Modal from "../components/Modal.js";
import Footer from "../components/Footer.js";
import './Home.scss';
// import star from "../img/star.png";
// import cart from "../img/cart.png";

// import { useEffect } from "react";
import { useEffect } from "react"; 
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { sync } from "../features/cart/cartSlice.js";
// import {setGoodsInCart, getWheels} from "../features/home/homeSlice";

function Home({ test,
  star,
  cart,
  add_to_cart,
  add_to_fav,
  actions,
  closeModal1,
  products,
  showModal,
  addToFav,
  addToCart,
  delFromCart,
  removeFromFav,
  star_added,
  shadow, visible1,
  show_add_to_cart_btn,
  del_from_cart, show_fav_star }) {  
  
  const dispatch = useDispatch();
  const cart_toolkit = useSelector((state) => state.cart.numberInCart);

  useEffect(() => { 
    dispatch(sync());
  },[dispatch]);
  
    return (
        <div className="App" onClick={test} id="test"> 
            <div className="main_header">
              <div className="main_header_in">
            <div className="main_header_title">
              {/* { cart_toolkit} */}
                  <p className="main_header_title_1">WHEELS</p>
                  <p className="main_header_title_2">and</p>
                  <p className="main_header_title_3">RIMS</p>
                  <p className="main_header_title_4">FOR YOUR CAR</p>
                </div>
                <div className="cart">
                <img className="cart_img" src={cart} alt="#" />
                <p className="cart_number ">{cart_toolkit}</p>
              </div>
              <div className="favorite">
                <img className="favorite_img" src={star} alt="#"/>
                <p className="favorite_number ">{add_to_fav}</p>
              </div>
              </div>
            </div>
          {/* {shadow && <div className="shadow" onClick={test} id="test"></div>}
          {visible1 && <Modal
            text="Are you sure you want to add this wheel to cart?"
            header="ADD PRODUCT TO CART"
            closeButton={true}
            actions={actions}
            closeModal={closeModal1}
              id={1} />} */}
        <Products products={products}
          onclick={showModal}
          onFavClick={addToFav}
          addToCart={addToCart}
          delFromCart={delFromCart}
          favRemove={removeFromFav}
          starAdded={star_added}
          show_add_to_cart_btn={show_add_to_cart_btn}
          del_from_cart={del_from_cart}
          //
          test={test}
          text="Are you sure you want to add this wheel to cart?"
          header="ADD PRODUCT TO CART"
          closeButton={true}
          actions={actions}
          closeModal={closeModal1}
          visible1={visible1}
          shadow={shadow}
          show_fav_star={show_fav_star}
        //
        />
            <Footer />
        </div>
      );   
    }

export default Home;

