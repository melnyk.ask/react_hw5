import Card from "../components/Card";
import CartForm from "../components/CartForm";
import "../pages/Cart.scss";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { sync } from "../features/cart/cartSlice";

function Cart({ onclick, onFavClick, favRemove, starAdded, show_add_to_cart_btn, delFromCart }) {

    const wheels = useSelector((state) => state.home.wheels); 
    const cart_arr = useSelector((state) => state.cart.cart )
    const dispatch = useDispatch();

    useEffect(() => { 
        dispatch(sync());
    },[dispatch]);

    // if (localStorage.getItem("products")) {
    if (wheels.wheels && localStorage.getItem("cart_toolkit")) {

        // let products = JSON.parse(localStorage.getItem("products"));
        // console.log("------>>", products);

        // const wheels = useSelector((state) => state.home.wheels);
        
                //////// let cart_arr = JSON.parse(localStorage.getItem("cart_toolkit"));
    
        // return (
        //     <div className="cart_page">
        //         <div className="cart_page_wrap">
        //             {wheels.wheels.map(
        //                 (card) => {
        //                     // if (localStorage.getItem(`cart_id${card.id}`)==="1") {
        //                     for (let i = 0; i < cart_arr.length; i++) {
        //                         if (card.id === cart_arr[i]) {
        //                             return (<Card key={card.id}
        //                                 id={card.id}
        //                                 color={card.color}
        //                                 url={card.url}
        //                                 title={card.title}
        //                                 price={card.price}
        //                                 onclick={() => { }}
        //                                 onFavClick={onFavClick}
        //                                 favRemove={favRemove}
        //                                 starAdded={starAdded}
        //                                 show_add_to_cart_btn={false}
        //                                 del_from_cart={true}
        //                                 delFromCart={delFromCart}
        //                             />);
        //                         }
        //                     }
        //                 }
        //             )}
        //         </div>
        //         <div className="cart_page_form">
        //             <CartForm />
        //         </div>
        //     </div>
        // );
        return (
            <div className="cart_page">
                <div className="cart_page_wrap">
                    {cart_arr.map(
                        (elem) => {
                            // if (localStorage.getItem(`cart_id${card.id}`)==="1") {
                            for (let i = 0; i < wheels.wheels.length; i++) {
                                if (elem === wheels.wheels[i].id) {
                                    return (<Card key={Math.floor(Math.random()*1000) * wheels.wheels[i].id}
                                        id={wheels.wheels[i].id}
                                        color={wheels.wheels[i].color}
                                        url={wheels.wheels[i].url}
                                        title={wheels.wheels[i].title}
                                        price={wheels.wheels[i].price}
                                        onclick={() => { }}
                                        onFavClick={onFavClick}
                                        favRemove={favRemove}
                                        starAdded={starAdded}
                                        show_add_to_cart_btn={false}
                                        del_from_cart={true}
                                        delFromCart={delFromCart}
                                    />);
                                }
                            }
                        }
                    )}
                </div>
                <div className="cart_page_form">
                    <CartForm />
                </div>
            </div>
        );
    }
    // else return null;
    else return (
        <div className="cart_page">
            <div className="cart_page_wrap">
            </div>
            <div className="cart_page_form">
                <CartForm />
            </div>
        </div>
    );
}

export default Cart;