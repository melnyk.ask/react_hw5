import Card from "../components/Card";
import "./Favorite.scss";
import { useSelector } from "react-redux";

function Favorite({ onclick, onFavClick, favRemove, starAdded, show_fav_star }) {

    const wheels = useSelector((state) => state.home.wheels);

    // if (localStorage.getItem("products")) {
        if (wheels.wheels) {

        // let products = JSON.parse(localStorage.getItem("products"));
        // console.log("------>>", products);

        
    
        return (
            <div className="favorite_page">
                <div className="favorite_page_wrap">
                    {/*{ products.wheels.map( */}
                    { wheels.wheels.map(
                                        (card) => {
                                            if (localStorage.getItem(`fav_id${card.id}`)==="1") { 
                                                return (<Card key={card.id}
                                                    id={card.id}
                                                    color={card.color}
                                                    url={card.url}
                                                    title={card.title}
                                                    price={card.price}
                                                    onclick={() => { }}
                                                    onFavClick={onFavClick}
                                                    favRemove={favRemove}
                                                    starAdded={starAdded}
                                                    show_add_to_cart_btn={false}
                                                    show_fav_star={ true}
                                                    />);
                                            }                
                                        }
                    )}
                </div>                
            </div>
            );
    }
    else return null;
}

export default Favorite;