import { Link } from "react-router-dom";
import "./Nav.scss";

function Nav() {    

    return (
        <div className="nav">
            <ul className="nav_ul">
                <li className="nav_ul_item home_item">
                    <Link className="nav_ul_item home_item" to="/">HOME</Link>
                </li>
                <li className="nav_ul_item cart_item">
                    <Link className="nav_ul_item cart_item" to="/cart">CART</Link>
                </li>
                <li className="nav_ul_item favorite_item">
                    <Link className="nav_ul_item favorite_item" to="/favorite">FAVORITE</Link>
                </li>
            </ul>
        </div>
    );
}

export default Nav;