import "./Modal.scss";

function Modal({id, header, closeModal, addToCart, closeButton, text, actions, add_to_cart}) {
    return (
        <div className="modal" id={id}>
            <div className="header_all">
                <p className="header">{header}</p>
                <div className="close" onClick={closeModal}>
                    {closeButton && <div className="line line1"></div>}
                    {closeButton && <div className="line line2"></div>}
                </div>
            </div>
            <p className="text">{text}</p>
            {/* <div>{actions}</div> */}
            <div className="actions">
                <div className="ok" onClick={() => { add_to_cart(id) }}>Add to cart</div>
                <div className="cancel" onClick={closeModal}>Cancel</div>
              </div>
        </div>
    )
}

export default Modal;