import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "../components/CartFofm.scss";
import { useDispatch } from "react-redux";
import { checkout } from "../features/cart/cartSlice";
import { useSelector } from "react-redux";


const initialValues = {
    name1: "",
    name2: "",
    age: "",
    address: "",
    phone: "",
};

const validationSchema = Yup.object({
    name1: Yup.string().min(3, "too few letters").required("enter your name, please"),
    name2: Yup.string().required("enter your surname, please"),
    age: Yup.number().required("enter your number, please"),  ///num!!
    address: Yup.string().required("enter your address, please"),
    phone: Yup.number().required("enter your phone, please"),
});

const component = (props) => {
    return (
        <div className="err_msg">
            {props.children}
        </div>
    );
}

function CartForm() {

    const dispatch = useDispatch();
    const wheels = useSelector((state) => state.home.wheels);
    const productsToBuy = useSelector((state) => state.cart.cart);
    let products = [];

    const onSubmit = (values, {resetForm}) => {

        for (let i = 0; i < productsToBuy.length; i++) { 
            for (let j = 0; j < wheels.wheels.length; j++) { 
                if (productsToBuy[i] === wheels.wheels[j].id) { 
                    products.push(wheels.wheels[j].title);
                }
            }
        }
       
        let checkOut = {
            name1: values.name1,
            name2: values.name1,
            age: values.age,
            address: values.address,
            phone: values.phone,
            products,
        }

        dispatch(checkout());
        console.log("checkOut", checkOut);
        resetForm({
            name1: "",
            name2: "",
            age: "",
            address: "",
            phone: "",
        });
}
    return (
        <Formik 
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        >
            <Form className="cart_form" >
                <div className="cart_form_div">
                    <label htmlFor="name1" className="cart_form_div_label">NAME</label>
                    <Field type="text" id="name1" name="name1" className="cart_form_div_field"/>
                    <ErrorMessage component={component} name="name1"/>
                </div>
                <div className="cart_form_div">
                    <label htmlFor="name2" className="cart_form_div_label">SURNAME</label>
                    <Field type="text" id="name2" name="name2" className="cart_form_div_field" />
                    <ErrorMessage component={component} name="name2"/>
                </div>
                <div className="cart_form_div">
                    <label htmlFor="age" className="cart_form_div_label">AGE</label>
                    <Field type="text" id="age" name="age" className="cart_form_div_field" />
                    <ErrorMessage render={()=> <div className="err_msg" >enter your age, please</div>} name="age"/>
                </div>
                <div className="cart_form_div">
                    <label htmlFor="address" className="cart_form_div_label">ADDRESS</label>
                    <Field type="text" id="address" name="address" className="cart_form_div_field" />
                    <ErrorMessage component={component} name="address"/>
                </div>
                <div className="cart_form_div">
                    <label htmlFor="phone" className="cart_form_div_label">PHONE</label>
                    <Field type="text" id="phone" name="phone" className="cart_form_div_field" />
                    <ErrorMessage render={()=> <div className="err_msg" >enter your phone, please</div>} name="phone"/>
                </div>
                <button type="submit" className="cart_form_btn">CHECKOUT</button>
            </Form>
        </Formik>
    );
}

export default CartForm;