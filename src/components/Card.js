import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import Modal from "./Modal";
import "./Card.scss";
import { setVisibleOn, setVisibleOff } from "../features/modal/modalSlice";
import { useDispatch, useSelector } from "react-redux";
import { add, remove, sync, checkout } from "../features/cart/cartSlice";

function Card({ id,
    color,
    url,
    title,
    price,
    onclick,
    onFavClick,
    addToCart,
    delFromCart,
    favRemove,
    starAdded,
    show_add_to_cart_btn,
    del_from_cart,
    //
    // test,
    text,
    header,
    closeButton,
    actions,
    show_fav_star
    // closeModal,
    // visible1,
    // shadow
    //
}) {
    
    let [addedStar, setAddedStar] = useState("0");
    let [toCart, setToCart] = useState("0");
    let [vis, setVis] = useState(false);
    // let [visible1, set_visible1] = useState(false);
    // let [shadow, set_shadow] = useState(false);

    const dispatch = useDispatch();
    const dispatch2 = useDispatch();

    const visible_modal = useSelector((state) => state.modal.visible);
    const visible_shadow = useSelector((state) => state.modal.shadow);
    let check = false;

//============favorite=================
    function add_to_fav(id) { 
        setAddedStar(addedStar="1");

        localStorage.setItem(`fav_id${id}`, "1");
        onFavClick(id);
    }

    function del_from_fav(id) { 
        setAddedStar(addedStar="0");

        // if (localStorage.getItem(`id${id}`)) { 
            localStorage.setItem(`fav_id${id}`, "0");
        // } 
        favRemove(id);
    }
//============favorite=================
//============cart=================
    function add_to_cart(id) { 
        setToCart(toCart="1");

        localStorage.setItem(`cart_id${id}`, "1");
        // console.log("======>>>>", `cart_id${id}`);

        dispatch2(add(id));

        addToCart();
        closeModal();
    }
    function delete_from_cart(id) { 
        setToCart(toCart="0");

        localStorage.setItem(`cart_id${id}`, "0");
        delFromCart();

        dispatch2(remove(id));
    }
//============cart=================
//======show_new_modal==========
function showModal() {
    // set_visible1(true);
    // set_shadow(true);
    check = true;
    setVis(true);
    dispatch(setVisibleOn());
    }
function closeModal() { 
    // set_visible1(false);
    // set_shadow(false);
    setVis(false);
    check = false;
    dispatch(setVisibleOff());
    }
function test(e) {
    // console.log(e.target.id);
    if (e.target.id === "test") { 
    //   set_visible1(false);
    //   set_shadow(false);
    setVis(false);
    check = false;
    dispatch(setVisibleOff());
    }
  }    
//======show_new_modal==========    
    
    

    useEffect(() => { 
        if (localStorage.getItem(`fav_id${id}`) !== null) { 
            setAddedStar(addedStar = localStorage.getItem(`fav_id${id}`));
        }
        localStorage.setItem(`fav_id${id}`, addedStar); 
    }, [addedStar]);
    // работает и без [addedStar].

    return (
        <>
            <div className="card" id={id} style={{ backgroundColor: color }}>
                {show_fav_star && <div className="card_favorite">
                    {(addedStar === "0") && <img className="card_favorite_icon" src="../img/star1_white.png" alt="star" onClick={() => { add_to_fav(id) }} />}
                    {(addedStar === "1") && <img className="card_favorite_icon" src="../img/star1_added.png" alt="star" onClick={() => { del_from_fav(id) }} />}
                </div>}
                {<div className="card_del" onClick={() => {delete_from_cart(id)}}>
                        {del_from_cart && <div className="line line1"></div>}
                        {del_from_cart && <div className="line line2"></div>}
                </div>}
                    <img className="card_img" src={url} alt="#" />
                    <h3 className="card_title">{title}</h3>
                    <p className="card_price">{price} $</p>
                {/* {show_add_to_cart_btn && <button className="card_add" onClick={onclick}>Add to cart</button>} */}
                    {show_add_to_cart_btn && <button className="card_add" onClick={showModal}>Add to cart</button>}
                
            </div>
            {/* {shadow && <div className="shadow" onClick={test} id="test"></div>} */}
            {visible_shadow && vis && <div className="shadow" onClick={test} id="test"></div>}
            {/* {visible1 && <Modal */}
            {visible_modal && vis && <Modal
            text={text}
            header={header}
            closeButton={closeButton}
            actions={actions}
            closeModal={closeModal}
            // addToCart={addToCart}
            add_to_cart={ () => { add_to_cart(id)}}
            id={id} />}
            {/* {shadow && <div className="shadow" onClick={test} id="test"></div>}
            {<Modal
            visible1={visible1}
            text={text}
            header={header}
            closeButton={closeButton}
            actions={actions}
            closeModal={closeModal}
            addToCart={addToCart}
            add_to_cart={ add_to_cart}
            id={id} />} */}
        </>
           
    )    
}

Card.propTypes = {
    id: PropTypes.number,
    color: PropTypes.string,
    url: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    onclick: PropTypes.func,
    onFavClick: PropTypes.func,
    favRemove: PropTypes.func,
    starAdded: PropTypes.number
}

export default Card;